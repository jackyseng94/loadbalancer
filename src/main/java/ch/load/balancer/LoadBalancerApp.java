package ch.load.balancer;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.stream.IntStream;

import ch.load.balancer.model.LoadBalancer;
import ch.load.balancer.model.Provider;
import ch.load.balancer.model.RandomLoadBalancer;
import ch.load.balancer.model.RoundRobinLoadBalancer;

public class LoadBalancerApp {

	public static final int NUM_OF_REQUESTS = 20;

	public static void main(String[] args) {
		List<Provider> providerPool = new ArrayList<>();
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());
		providerPool.add(new Provider());

		LoadBalancer random = new RandomLoadBalancer(providerPool);
		Timer timer = new Timer();
		timer.schedule(random, 0, 6000);

		LoadBalancer roundRobbin = new RoundRobinLoadBalancer(providerPool);
		timer.schedule(roundRobbin, 0, 4000);
	}

	public static void simulateConcurrentClientRequest(LoadBalancer loadBalancer) {
		IntStream.range(0, NUM_OF_REQUESTS).parallel().forEach(i -> System.out.println("IP: " + loadBalancer.get()
				+ " --- Request from Client: " + i + " --- [Thread: " + Thread.currentThread().getName() + "]"));
	}
}
