package ch.load.balancer.model;

import java.util.Random;
import java.util.UUID;
import java.util.function.Supplier;

public class Provider implements Supplier<String> {

	private String providerId;
	private Random random;
	private boolean isIncluded;
	private int nbConsecutiveIsAlive;

	public Provider() {
		super();
		this.providerId = UUID.randomUUID().toString();
		this.random = new Random();
		this.isIncluded = true;
	}

	public String get() {
		return this.providerId;
	}

	public boolean check() {
		boolean isAlive = random.nextDouble() > 0.25;
		nbConsecutiveIsAlive = isAlive ? ++nbConsecutiveIsAlive : 0;

		return isAlive;
	}

	public boolean isIncluded() {
		return isIncluded;
	}

	public void setIncluded(boolean isIncluded) {
		this.isIncluded = isIncluded;
	}

	public int getNbConsecutiveIsAlive() {
		return nbConsecutiveIsAlive;
	}

}
