package ch.load.balancer.model;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomLoadBalancer extends LoadBalancer {

	public RandomLoadBalancer(List<Provider> providerList) {
		super(providerList, "Random");
	}

	public String get() {
		List<Provider> includedProviders = providerList.stream().filter(Provider::isIncluded)
				.collect(Collectors.toList());
		if (includedProviders.isEmpty()) {
			return "";
		}
		Random random = new Random();
		return includedProviders.get(random.nextInt(includedProviders.size())).get();
	}
}
