package ch.load.balancer.model;

import java.util.Collections;
import java.util.List;
import java.util.TimerTask;

import ch.load.balancer.LoadBalancerApp;

public abstract class LoadBalancer extends TimerTask {
	
	private static final int NB_SUCCESSFUL_HEART_BEAT = 2;
	
	protected String name;
	protected List<Provider> providerList;

	protected LoadBalancer(List<Provider> providerList, String name) {
		super();
		if (providerList.size() > 10) {
			throw new IllegalArgumentException("Maximum number of provider is 10.");
		}
		this.providerList = Collections.unmodifiableList(providerList);
		this.name = name;
	}

	public abstract String get();

	public void includeProvider(Provider provider) {
		if (provider == null) {
			throw new IllegalArgumentException("Provider can't be null.");
		}
		provider.setIncluded(true);
	}

	public void excludeProvider(Provider provider) {
		if (provider == null) {
			throw new IllegalArgumentException("Provider can't be null.");
		}
		provider.setIncluded(false);
	}

	public void run() {
		providerList.stream().forEach(provider -> {
			if (!provider.check()) {
				this.excludeProvider(provider);
			} else if (provider.getNbConsecutiveIsAlive() == NB_SUCCESSFUL_HEART_BEAT) {
				this.includeProvider(provider);
			}
		});
		printNextTurn();
		LoadBalancerApp.simulateConcurrentClientRequest(this);
	}
	
	public void printNextTurn() {
		System.out.println();
		System.out.println("---");
		System.out.println("Clients starts to send requests to " + name + " Load Balancer");
		System.out.println("---");
	}

}
