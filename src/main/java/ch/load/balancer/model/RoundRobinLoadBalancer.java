package ch.load.balancer.model;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class RoundRobinLoadBalancer extends LoadBalancer {

	private int counter = 0;
	private final ReentrantLock reentrantLock;

	public RoundRobinLoadBalancer(List<Provider> providerList) {
		super(providerList, "Round Robin");
		reentrantLock = new ReentrantLock();
	}

	public String get() {
		reentrantLock.lock();
		try {
			List<Provider> includedProviders = providerList.stream().filter(Provider::isIncluded).collect(Collectors.toList());
			if (includedProviders.isEmpty()) {
				return "";
			} else if (counter >= includedProviders.size()) {
				counter = 0;
			}
			return includedProviders.get(counter++).get();
		} finally {
			reentrantLock.unlock();
		}
	}
}
